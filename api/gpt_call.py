from fastapi import FastAPI, Request
from pydantic import BaseModel
from typing import List
import httpx
from dotenv import load_dotenv
import os

load_dotenv()
app = FastAPI()

# Define the OpenAI API endpoint and your API key
OPENAI_API_ENDPOINT = "https://api.openai.com/v1/engines/gpt-3.5/completions"
OPENAI_API_KEY = os.environ.get("YOUR_OPENAI_KEY")

# Define the request model for the FastAPI endpoint
class GPTRequest(BaseModel):
    prompt: str
    max_tokens: int = 100

# Define the response model for the FastAPI endpoint
class GPTResponse(BaseModel):
    choices: List[dict]

@app.post("/chatgpt/")
async def chat_with_gpt(request: GPTRequest):
    async with httpx.AsyncClient() as client:
        headers = {
            "Authorization": f"Bearer {OPENAI_API_KEY}",
            "Content-Type": "application/json",
        }
        payload = {
            "prompt": request.prompt,
            "max_tokens": request.max_tokens,
        }
        response = await client.post(OPENAI_API_ENDPOINT, headers=headers, json=payload)
        response_data = response.json()

        return GPTResponse(choices=response_data["choices"])