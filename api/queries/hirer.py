from .client import Queries
from models.hirer import (
    HirerProfile,
    HirerProfileIn,
    HirerProfileOut,
    HirerProfileUpdateForm
)


class DuplicateError(ValueError):
    pass


class NoProfileError(ValueError):
    pass


class HirerQueries(Queries):
    DB_NAME = "searching"
    COLLECTION = "hirers"

    def get(self, account_id: str) -> HirerProfile:
        props = self.collection.find_one({"account_id": account_id})
        if not props:
            return {"message": "Searcher profile does not exist"}
        props["id"] = str(props["_id"])
        return HirerProfile(**props)

    def create(self, info: HirerProfileIn, account_data) -> HirerProfile:
        props = info.dict()
        props["account_id"] = account_data["id"]
        props["account_email"] = account_data["email"]
        if self.collection.find_one({"account_id": props["account_id"]}):
            raise DuplicateError()
        if self.collection.find_one({"account_email": props["account_email"]}):
            raise DuplicateError()

        self.collection.insert_one(props)

        props["id"] = str(props["_id"])

        return HirerProfile(**props)

    def update(
        self, info: HirerProfileUpdateForm, account_email: str
    ) -> HirerProfileOut:
        props = self.collection.find_one({"account_email": account_email})
        try:
            props["id"] = str(props["_id"])
        except TypeError:
            raise NoProfileError()
        for k, v in info.dict().items():
            if v is None or v == "":
                pass
            else:
                props[k] = v

        self.collection.update_one(
            {"account_email": account_email},
            {
                "$set": {
                    "company_name": props["company_name"],
                    "job_posted": props["job_posted"],
                    "applicants": props["applicants"],
                    "selections": props["selections"],
                }
            },
        )

        return HirerProfileOut(**props)

    def delete(self, account_email) -> dict:
        status = self.collection.delete_one({"account_email": account_email})
        if status.deleted_count:
            return {"message": "profile deleted successfully"}
        else:
            return {"message": "profile deletion failed"}
