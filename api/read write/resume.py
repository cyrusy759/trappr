import os
from models.resume import Resume
from fastapi import FastAPI, File, UploadFile
from pydantic import BaseModel
from pdf_extractor import extract_text

app = FastAPI()

class Resume(BaseModel):
    file: UploadFile

@app.post("/api/scan_resume/")
async def scan_resume(resume: Resume):
    upload_folder = "resumes"

    # Create the folder if it doesn't exist
    if not os.path.exists(upload_folder):
        os.makedirs(upload_folder)

    # Save the uploaded resume
    resume_path = os.path.join(upload_folder, resume.file.filename)
    with open(resume_path, "wb") as f:
        f.write(await resume.file.read())

    # Extract text from the uploaded resume
    text = extract_text(resume_path)

    # Process the text content as needed (e.g., extract contact details, education, etc.)

    return {"message": "Resume uploaded and scanned successfully."}