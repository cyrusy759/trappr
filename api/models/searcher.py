from pydantic import BaseModel
from typing import List

class SearcherProfile(BaseModel):
    account_id: str
    account_email: str
    id: str
    first_name: str
    last_name: str
    jobs_applied: List[str]
    interviews: List[str]
    offers: List[str]


class SearcherProfileIn(BaseModel):
    first_name: str
    last_name: str
    jobs_applied: List[str]
    interviews: List[str]
    offers: List[str]
    

class SearcherProfileOut(BaseModel):
    account_email: str
    first_name: str
    last_name: str
    jobs_applied: List[str]
    interviews: List[str]
    offers: List[str]


class SearcherProfileUpdateForm(BaseModel):
    first_name: str
    last_name: str
    jobs_applied: List[str]
    interviews: List[str]
    offers: List[str]
    

