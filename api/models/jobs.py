from pydantic import BaseModel
from typing import List, Optional


class Jobs(BaseModel):
    account_id: str
    account_email: str
    id: str
    company_name: str
    compensation: int
    benefits: Optional[str]
    hours: int
    location: str
    qualifications: str
    experience: str
    education: str
    description: str
    tags: List[str]
    keywords: List[str]
    upload_date: dict


class JobsIn(BaseModel):
    company_name: str
    compensation: int
    benefits: Optional[str]
    hours: int
    location: str
    qualifications: str
    experience: str
    education: str
    description: str
    tags: List[str]
    keywords: List[str]
    upload_date: dict


class JobsOut(BaseModel):
    account_email: str
    company_name: str
    compensation: int
    benefits: Optional[str]
    hours: int
    location: str
    qualifications: str
    experience: str
    education: str
    description: str
    tags: List[str]
    keywords: List[str]
    upload_date: dict