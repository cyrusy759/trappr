from pydantic import BaseModel
from typing import List


class HirerProfile(BaseModel):
    account_id: str
    account_email: str
    id: str
    company_name: str
    jobs_posted: List[str]
    applicants: List[str]
    selections: List[str]


class HirerProfileIn(BaseModel):
    company_name: str
    jobs_posted: List[str]
    applicants: List[str]
    selections: List[str]


class HirerProfileOut(BaseModel):
    account_email: str
    company_name: str
    jobs_posted: List[str]
    applicants: List[str]
    selections: List[str]


class HirerProfileUpdateForm(BaseModel):
    company_name: str
    jobs_posted: List[str]
    applicants: List[str]
    selections: List[str]

