from fastapi import UploadFile
from typing import List
from pydantic import BaseModel
from datetime import datetime


class Resume(BaseModel):
    account_id: str
    account_email: str
    resume_name: str
    tags: List[str]
    keywords: List[str]
    upload_date: dict
    file: UploadFile

