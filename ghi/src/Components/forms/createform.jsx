import { useState } from "react";
import axios from "axios";

function SignUpForm() {
    const [account, setAccount] = useState({
        username: '',
        password: '',
        email: '',
        first_name: '',
        last_name: '',
        avatar: '',
    })
    const handleFormChange = (event) => {
        setAccount({...account, [event.target.name]: event.target.value});
    }
    const handleSubmit = async(event) => {
        event.preventDefault();
        axios.post(`${process.env.REACT_APP_API_HOST}/api/accounts`, account)
    }
    return(
        <div>
            <label>username</label>
            <input type='text'
                onChange={handleFormChange} 
                name='username' 
                value={account.username}></input>
            <br/>
            <label>password</label>
            <input type='text'
                onChange={handleFormChange} 
                name='password' 
                value={account.password}></input>
            <br/>
            <label>email</label>
            <input type='text'
                onChange={handleFormChange} 
                name='email' 
                value={account.email}></input>
            <br/>
            <label>first name</label>
            <input type='text'
                onChange={handleFormChange} 
                name='first_name' 
                value={account.first_name}></input>
            <br/>
            <label>last_name</label>
            <input type='text'
                onChange={handleFormChange} 
                name='last_name' 
                value={account.last_name}></input>
            <br/>
            <label>avatar</label>
            <input type='text'
                onChange={handleFormChange} 
                name='avatar' 
                value={account.avatar}></input>
            <br/>
            <button onClick={handleSubmit}>submit</button>
        </div>
    );
}

export default SignUpForm;