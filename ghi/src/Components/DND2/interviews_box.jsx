import React from 'react';
import './box.css';

const InterviewsBox = () => {
  const handleDragOver = (event) => {
    event.preventDefault();
  };

  const handleDrop = (event) => {
    event.preventDefault();
    const data = event.dataTransfer.getData('application/json');
    console.log('Dropped:', data);

    
  };

  return (
    <div className="drag-into-box" onDragOver={handleDragOver} onDrop={handleDrop}>
      Interviews
    </div>
  );
};

export default InterviewsBox;