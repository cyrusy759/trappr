import React from 'react';
import GPTChat from './Gpt';

function GPTApp() {
  return (
    <div>
      <GPTChat />
    </div>
  );
}

export default GPTApp;