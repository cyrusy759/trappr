import React, { useState } from 'react';
import axios from 'axios';

const GPTChat = () => {
  const [input, setInput] = useState('');
  const [output, setOutput] = useState('');

  const handleChange = (event) => {
    setInput(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post('/api/chatgpt/', { prompt: input, max_tokens: 100 });
      setOutput(response.data.choices[0].text);
    } catch (error) {
      console.error('Error fetching GPT response:', error);
    }
  };

  return (
    <div>
      <h1>GPT Chat</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Enter your message:
          <input type="text" value={input} onChange={handleChange} />
        </label>
        <button type="submit">Send</button>
      </form>
      <div>
        <strong>Response:</strong>
        <p>{output}</p>
      </div>
    </div>
  );
};

export default GPTChat;