import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

export const store = configureStore({
	reducer: {
		[authApiSlice.reducerPath]: authApiSlice.reducer,
		
	},
	middleware: getDefaultMiddleware => {
    return getDefaultMiddleware()
		.concat(authApiSlice.middleware)
	},
});

setupListeners(store.dispatch);
