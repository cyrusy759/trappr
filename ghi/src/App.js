import { BrowserRouter, Routes, Route } from "react-router-dom";
import Main from "./Components/Main/Main.jsx";
import GPTApp from "./Components/GPT/gpt_page.jsx";
import DND2App from "./Components/DND2/dnd_test.jsx";
import SignUpForm from "./Components/forms/createform.jsx";
import LoginForm from "./Components/forms/loginform.jsx";
import "./App.css";

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, '');


  return (
    <BrowserRouter basename={basename}>
      <div className="container">
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/gpt" element={<GPTApp />} />
          <Route path ="/dnd" element ={<DND2App />} />
          <Route path="signup" element ={<SignUpForm />} />
          <Route path="login" element = {<LoginForm />} />
        </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
